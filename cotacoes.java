/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sources;

import java.util.ArrayList;

/**
 *
 * @author matheus
 */
public class cotacoes {
    ArrayList<String> titulos = new ArrayList<>();
    
    public void addTitulo(String titulo){
        titulos.add(titulo);
    }
    
    public void removerTitulo(int pos){
        titulos.remove(pos);
    }
    
    public void removerTitulo(String titulo){
        titulos.remove(titulo);
    }
    
    public void alterar(int pos, String novo){
        titulos.set(pos, novo);
    }
    
}
